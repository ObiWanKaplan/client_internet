package ru.asstra.servicedashboard.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.*
import ru.asstra.servicedashboard.services.IService
import java.util.*

@RestController
@RequestMapping("/dashboard")
class CController
{
    @Autowired
    lateinit var service           : IService


    /****************************************************************************************************
     * Запрос возвращает список организаций для администратора                                          *
     * Запрос GET.                                                                                      *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            produces                        = [MediaType.APPLICATION_JSON_VALUE],
            path                            = ["/test"],
            params                          = ["workspace_id"]
            )
    @ResponseBody
    fun test(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID
    )                                       : String
    {
        return service.get(authentication, workspaceId)
    }
}