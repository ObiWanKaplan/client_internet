package ru.asstra.servicedashboard.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*


/****************************************************************************************************
 * Класс содержит все используемые запросы к API через RestTemplate                                 *
 * @author Каплан Андрей,  13.11.2019.                                                              *
 ***************************************************************************************************/
@Service
class CRestTemplate : IRestTemplate {

    @Autowired
    lateinit var restTemplate               : RestTemplate

    val apiPath                             = "http://localhost:13001/project/api"

    override fun get(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : String
    {
        //Header
        val token = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers = HttpHeaders()
        headers.setBearerAuth(token)
        val entity = HttpEntity<Any>(headers)
        //Parameters
        val params = HashMap<String, UUID>()
        params["workspace_id"] = workspaceId
        //URI
        val endpoint = URI.create("$apiPath/test")

        return restTemplate.exchange(endpoint.toString(), HttpMethod.GET, entity, String::class.java, params).body!!
    }
}