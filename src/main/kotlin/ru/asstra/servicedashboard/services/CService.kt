package ru.asstra.servicedashboard.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import java.util.*
import ru.asstra.servicedashboard.rest.IRestTemplate


/**************************************************************************************************************
 * Класс реализует все методы бизнес-логики, которые могут применятся для формирования данных в виде дашборда *
 * @author Теплюк О.В. 2019 0725                                                                              *
 **************************************************************************************************************/
@Service
class CService                     : IService
{
    @Autowired
    lateinit var restTemplate               : IRestTemplate

    override fun get(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : String
    {
        return restTemplate.get(authentication, workspaceId)
    }
}