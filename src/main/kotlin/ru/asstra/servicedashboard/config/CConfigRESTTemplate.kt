package ru.asstra.servicedashboard.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class CConfigRESTTemplate {
    @Bean
    fun getRestTemplate()                   : RestTemplate
    {
        return RestTemplate()
    }
}