package ru.swiftgroup.project.api.utils.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class CSerializerLocalDateTime               : JsonSerializer<LocalDateTime>()
{
    override fun serialize(
            value                           : LocalDateTime?,
            gen                             : JsonGenerator?,
            serializers                     : SerializerProvider?)
    {
        val formatter                       = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        gen!!.writeString(value!!.format(formatter))
    }

}