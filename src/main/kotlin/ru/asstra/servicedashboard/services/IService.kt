package ru.asstra.servicedashboard.services

import org.springframework.security.oauth2.provider.OAuth2Authentication
import java.util.*

/********************************************************************************************************
 * Интерфейс содержит заголовки методов формирования данных для дашборда.                               *
 * @author Теплюк О.В. 2019 0726.                                                                       *
 *******************************************************************************************************/
interface IService
{
    fun get(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : String

}