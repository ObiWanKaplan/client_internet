package ru.asstra.servicedashboard.rest

import org.springframework.security.oauth2.provider.OAuth2Authentication
import java.util.*

interface IRestTemplate {

    fun get(
            authentication                  : OAuth2Authentication,
            workspaceId: UUID
    )                                       : String
}