package ru.asstra.servicedashboard.utils

import org.apache.poi.util.Beta
import org.apache.poi.xddf.usermodel.XDDFShapeProperties
import org.apache.poi.xddf.usermodel.chart.*
import org.openxmlformats.schemas.drawingml.x2006.chart.*


@Beta
class CXDDFDoughnutChartData(parent: XDDFChart, private val chart: CTDoughnutChart) : XDDFChartData() {

    var firstSliceAngle: Int?
        get() = if (chart.isSetFirstSliceAng) {
            chart.firstSliceAng.getVal()
        } else {
            null
        }
        set(angle) {
            if (angle == null) {
                if (chart.isSetFirstSliceAng) {
                    chart.unsetFirstSliceAng()
                }
            } else {
                require(!(angle < 0 || 360 < angle)) { "angle must be between 0 and 360" }
                if (chart.isSetFirstSliceAng) {
                    chart.firstSliceAng.setVal(angle)
                } else {
                    chart.addNewFirstSliceAng().setVal(angle)
                }
            }
        }


    fun getHoleSize(): Short? {
        return if (chart.isSetHoleSize) {
            chart.holeSize.getVal()
        } else {
            null
        }
    }

        fun setHoleSize (size:Short) {
            if (size == null) {
                if (chart.isSetHoleSize) {
                    chart.unsetHoleSize()
                }
            } else {
                require(!(size < 0 || 100 < size)) { "size must be between 0 and 100" }
                if (chart.isSetHoleSize) {
                    chart.holeSize.setVal(size)
                } else {
                    chart.addNewHoleSize().setVal(size)
                }
            }
        }

    init {
        for (series in chart.serList) {
            this.series.add(Series(series, series.cat, series.getVal()))
        }
    }


    protected fun removeCTSeries(n: Int) {
        chart.removeSer(n)
    }


    fun setVaryColors(varyColors: Boolean?) {
        if (varyColors == null) {
            if (chart.isSetVaryColors) {
                chart.unsetVaryColors()
            }
        } else {
            if (chart.isSetVaryColors) {
                chart.varyColors.setVal(varyColors)
            } else {
                chart.addNewVaryColors().setVal(varyColors)
            }
        }
    }

    override fun setVaryColors(varyColors: Boolean) {

    }

    override fun addSeries(category: XDDFDataSource<*>,
                           values: XDDFNumericalDataSource<out Number>): XDDFChartData.Series {
        val index = this.series.size.toLong()
        val ctSer = this.chart.addNewSer()
        ctSer.addNewCat()
        ctSer.addNewVal()
        ctSer.addNewIdx().setVal(index)
        ctSer.addNewOrder().setVal(index)
        val added = Series(ctSer, category, values)
        this.series.add(added)
        return added
    }

    inner class Series : XDDFChartData.Series {
        private var series: CTPieSer? = null

        var explosion: Long?
            get() = if (series!!.isSetExplosion) {
                series!!.explosion.getVal()
            } else {
                null
            }
            set(explosion) {
                if (explosion == null) {
                    if (series!!.isSetExplosion) {
                        series!!.unsetExplosion()
                    }
                } else {
                    if (series!!.isSetExplosion) {
                        series!!.explosion.setVal(explosion)
                    } else {
                        series!!.addNewExplosion().setVal(explosion)
                    }
                }
            }

        constructor(series: CTPieSer, category: XDDFDataSource<*>,
                              values: XDDFNumericalDataSource<out Number>) : super(category, values) {
            this.series = series
        }

        constructor(series: CTPieSer, category: CTAxDataSource, values: CTNumDataSource) : super(XDDFDataSourcesFactory.fromDataSource(category), XDDFDataSourcesFactory.fromDataSource(values)) {
            this.series = series
        }

        override fun getSeriesText(): CTSerTx {
            return if (series!!.isSetTx) {
                series!!.tx
            } else {
                series!!.addNewTx()
            }
        }

        override fun setShowLeaderLines(showLeaderLines: Boolean) {
            if (!series!!.isSetDLbls) {
                series!!.addNewDLbls()
            }
            if (series!!.dLbls.isSetShowLeaderLines) {
                series!!.dLbls.showLeaderLines.setVal(showLeaderLines)
            } else {
                series!!.dLbls.addNewShowLeaderLines().setVal(showLeaderLines)
            }
        }

        override fun getShapeProperties(): XDDFShapeProperties? {
            return if (series!!.isSetSpPr) {
                XDDFShapeProperties(series!!.spPr)
            } else {
                null
            }
        }

        override fun setShapeProperties(properties: XDDFShapeProperties?) {
            if (properties == null) {
                if (series!!.isSetSpPr) {
                    series!!.unsetSpPr()
                }
            } else {
                if (series!!.isSetSpPr) {
                    series!!.spPr = properties.xmlObject
                } else {
                    series!!.addNewSpPr().set(properties.xmlObject)
                }
            }
        }

        override fun getAxDS(): CTAxDataSource {
            return series!!.cat
        }

        override fun getNumDS(): CTNumDataSource {
            return series!!.getVal()
        }
    }
}